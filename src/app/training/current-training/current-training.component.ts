import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { StopTrainingComponent } from './stop-training/stop-training.component';

@Component({
  selector: 'app-current-training',
  templateUrl: './current-training.component.html',
  styleUrls: ['./current-training.component.css'],
})
export class CurrentTrainingComponent implements OnInit {
  color: ThemePalette = 'primary';
  value: number = 0;
  timer: number = 0;
  @Output() trainingExit = new EventEmitter();
  disableButton: boolean = false;

  constructor(private dialog: MatDialog) {}

  openDialog() {
    const dialogRef = this.dialog.open(StopTrainingComponent, {
      width: '250px',
      data: { progress: this.value },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.trainingExit.emit();
      } else {
        this.startOrResumeTimer();
      }
    });
  }

  ngOnInit(): void {
    this.startOrResumeTimer();
  }

  onStop() {
    clearInterval(this.timer);
    this.openDialog();
  }

  startOrResumeTimer() {
    this.timer = setInterval(() => {
      this.value += 10;
      if (this.value >= 100) {
        clearInterval(this.timer);
        this.disableButton = true;
      }
    }, 1000);
  }
}
