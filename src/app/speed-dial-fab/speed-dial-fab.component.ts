import { Component, OnInit } from '@angular/core';
import { speedDialFabAnimations } from './speed-dial-fab.animations';

interface Iicon {
  icon: string;
}

@Component({
  selector: 'app-speed-dial-fab',
  templateUrl: './speed-dial-fab.component.html',
  styleUrls: ['./speed-dial-fab.component.css'],
  animations: speedDialFabAnimations,
})
export class SpeedDialFabComponent implements OnInit {
  fabButtons: Iicon[] = [
    {
      icon: 'timeline',
    },
    {
      icon: 'view_headline',
    },
    {
      icon: 'room',
    },
    {
      icon: 'lightbulb_outline',
    },
    {
      icon: 'lock',
    },
  ];
  buttons: Iicon[] = [];
  fabTogglerState = 'inactive';
  constructor() {}

  ngOnInit(): void {}

  showItems() {
    this.fabTogglerState = 'active';
    this.buttons = this.fabButtons;
  }

  hideItems() {
    this.fabTogglerState = 'inactive';
    this.buttons = [];
  }

  onToggleFab() {
    this.buttons.length ? this.hideItems() : this.showItems();
  }
}
